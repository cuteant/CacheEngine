﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using CacheEngine;
using CacheEngine.Memcache;

namespace CacheEngineDemo1
{
    public class DemoRepository
    {
        public int HitCounter = 0;

        [CacheEngine(Second = 12)]
        public int GetCustomersAgeBy(string name)
        {
            Console.WriteLine("直接读取！");
            HitCounter += 1;
            int age = -1;
            XmlDocument doc = new XmlDocument();
            doc.Load("Data.xml");

            XmlNode node = doc.SelectSingleNode(string.Format("/Customers/Customer [Name='{0}']", name));
            if (node != null)
            {

                age = Convert.ToInt32(node["Age"].InnerText);

            }
            else
                throw new Exception(string.Format("找不到这个名字的字段 '{0}'", name));

            return age;
        }
    }
}
