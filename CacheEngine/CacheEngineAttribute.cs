﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CacheEngine
{
    /// <summary>缓存引擎的自定义属性</summary>

    public class CacheEngineAttribute : Attribute
    {
        private float _Hours;

        private float _Minute;

        private float _Second;
        /// <summary>缓存引擎类型</summary>
        public Type CacheEngineType { get; set; }
        /// <summary></summary>
        public Type KeyGeneratorType { get; set; }
        public IExpirationTime ExpirationTime { get; protected set; }

        /// <summary>缓存时间小时</summary>
        public float Hours
        {
            get { return _Hours; }
            set { this.ExpirationTime = CacheEngine.ExpirationTime.HoursFromNow(value); _Hours = value; }
        }

        /// <summary>缓存分钟为单位</summary>
        public float Minute
        {
            get { return _Minute; }
            set { this.ExpirationTime = CacheEngine.ExpirationTime.HoursFromNow(value / 60); _Minute = value; }
        }

        /// <summary>缓存秒为单位</summary>
        public float Second
        {
            get { return _Second; }
            set { this.ExpirationTime = CacheEngine.ExpirationTime.HoursFromNow(value / 60 / 60); _Second = value; }
        }





        /// <summary>默认构造</summary>
        public CacheEngineAttribute()
        {
            CacheEngineType = typeof(MemoryCacheEngine);
            KeyGeneratorType = typeof(HashCodeKeyGenerator);
            ExpirationTime = CacheEngine.ExpirationTime.HoursFromNow(1);
        }
    }
}
