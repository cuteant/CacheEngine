﻿namespace Ha666.Redis
{
    class CONST_VALURES
    {

        public const string REDIS_COMMAND_SELECT = "SELECT";
        public const string REDIS_COMMAND_PING = "PING";
        public const string REDIS_COMMAND_EXPIRE = "EXPIRE";
        public const string REDIS_COMMAND_RANDOMKEY = "RANDOMKEY";
        public const string REDIS_COMMAND_HEXISTS = "HEXISTS";
        public const string REDIS_COMMAND_HKEYS = "HKEYS";
        public const string REDIS_COMMAND_HVALS = "HVALS";
        public const string REDIS_COMMAND_HGETALL = "HGETALL";
        public const string REDIS_COMMAND_HMGET = "HMGET";
        public const string REDIS_COMMAND_HGET = "HGET";
        public const string REDIS_COMMAND_HDEL = "HDEL";
        public const string REDIS_COMMAND_HLEN = "HLEN";
        public const string REDIS_COMMAND_HMSET = "HMSET";
        public const string REDIS_COMMAND_HSET = "HSET";
        public const string REDIS_COMMAND_HSETNX = "HSETNX";
        public const string REDIS_COMMAND_MGET = "MGET";
        public const string REDIS_COMMAND_GET = "GET";
        public const string REDIS_COMMAND_SET = "SET";
        public const string REDIS_COMMAND_INCR = "INCR";
        public const string REDIS_COMMAND_DECR = "DECR";
        public const string REDIS_COMMAND_DEL = "DEL";
        public const string REDIS_COMMAND_LPOP = "LPOP";
        public const string REDIS_COMMAND_RPOP = "RPOP";
        public const string REDIS_COMMAND_LPUSH = "LPUSH";
        public const string REDIS_COMMAND_LSET = "LSET";
        public const string REDIS_COMMAND_RPUSH = "RPUSH";
        public const string REDIS_COMMAND_LRANGE = "LRANGE";
        public const string REDIS_COMMAND_LINDEX = "LINDEX";
        public const string REDIS_COMMAND_LLEN = "LLEN";
        public const string REDIS_COMMAND_KEYS = "KEYS";
        public const string REDIS_COMMAND_MSET = "MSET";
        public const string REDIS_COMMAND_INFO = "INFO";
        public const string REDIS_COMMAND_SORT = "SORT";
        public const string REDIS_COMMAND_TIME = "TIME";
        public const string REDIS_COMMAND_ZADD = "ZADD";
        public const string REDIS_COMMAND_ZCARD = "ZCARD";
        public const string REDIS_COMMAND_ZCOUNT = "ZCOUNT";
        public const string REDIS_COMMAND_ZRANGE = "ZRANGE";
        public const string REDIS_COMMAND_ZRANGEBYSCORE = "ZRANGEBYSCORE";
        public const string REDIS_COMMAND_ZREVRANGE = "ZREVRANGE";
        public const string REDIS_COMMAND_ZRANK = "ZRANK";
        public const string REDIS_COMMAND_ZREVRANK = "ZREVRANK";
        public const string REDIS_COMMAND_ZREM = "ZREM";
        public const string REDIS_COMMAND_ZSCORE = "ZSCORE";
        public const string REDIS_COMMAND_SADD = "SADD";
        public const string REDIS_COMMAND_SCARD = "SCARD";
        public const string REDIS_COMMAND_SISMEMBER = "SISMEMBER";
        public const string REDIS_COMMAND_SMEMBERS = "SMEMBERS";
        public const string REDIS_COMMAND_SREM = "SREM";

    }
}
